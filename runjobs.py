#!/usr/bin/env python3
import sys
from ecalautoctrl import HTCHandler

if __name__ == '__main__':
    handler = HTCHandler(appname='MYAPP',
                         pd='/MYPD',
                         tier='RAW')

    ret = handler(wflow='my-workflow')

    sys.exit(ret)

